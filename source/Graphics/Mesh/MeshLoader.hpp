#ifndef SOURCE_GRAPHICS_MESH_MESHLOADER_HPP
#define SOURCE_GRAPHICS_MESH_MESHLOADER_HPP

#include <assimp/Importer.hpp>
#include <assimp/PostProcess.h>
#include <assimp/Scene.h>

#include "Mesh.hpp"

class MeshLoader
{
public:
	MeshLoader();
	~MeshLoader();
	StaticMesh LoadStaticMesh(const char* filepath);

private:
	StaticMesh GenerateBuffers();
	Assimp::Importer m_importer;
	const aiScene* m_scene;
};

#endif