#version 440

layout (location = 0) in vec4 position;
layout (location = 1) in vec4 normal;
layout (location = 2) in vec2 uv;

uniform mat4 projection;
uniform mat4 view;

out vec4 posFS;
out vec4 normalFS;
out vec2 uvFS;

void main()
{
	vec4 instancePos = projection * view * position;

	posFS = instancePos;
	normalFS = normal;
	uvFS = uv;

	gl_Position = instancePos;
}