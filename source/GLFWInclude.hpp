#ifndef SOURCE_GLFW_GLFWINCLUDE_HPP
#define SOURCE_GLFW_GLFWINCLUDE_HPP

#include <GLFW/glew.h>
#include <GLFW/glfw3.h>

#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

#include <GLFW/glfw3native.h>

#endif