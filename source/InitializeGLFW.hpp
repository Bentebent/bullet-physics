#ifndef SOURCE_INITIALIZEGLFW_HPP
#define SOURCE_INITIALIZEGLFW_HPP

#include <iostream>
#include "GLFWInclude.hpp"

void InitializeGLFW(GLFWwindow** window, int windowWidth, int windowHeight)
{
	glfwInit();
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_FALSE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
	
	(*window) = glfwCreateWindow(windowWidth, windowHeight, "Bullet Physics", nullptr, nullptr);
	glfwMakeContextCurrent(*window);
	
	std::cout << "OpenGL version: " << glfwGetWindowAttrib((*window), GLFW_CONTEXT_VERSION_MAJOR) << "." << glfwGetWindowAttrib((*window), GLFW_CONTEXT_VERSION_MINOR) << std::endl;
	
	// if 1 then limits system to max 60 fps!
	glfwSwapInterval(0);
	glClearColor(0, 0, 0, 0);
	
	glewExperimental = true;
	glewInit();
	
	glDisable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
	
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glfwSwapBuffers(*window);
	glfwPollEvents();
}


#endif