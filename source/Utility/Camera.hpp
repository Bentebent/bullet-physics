#ifndef SOURCE_UTILITY_CAMERA_HPP
#define SOURCE_UTILITY_CAMERA_HPP

#include <glm/glm.hpp>

class Camera
{
public:
	Camera();
	~Camera();

	void SetProjectionMatrix(float fov, float nearClip, float farClip, int screenWidth, int screenHeight);

	glm::mat4 GetProjectionMatrix();
	glm::mat4 GetViewMatrix();

private:
	float m_fieldOfView;
	float m_nearClip;
	float m_farClip;
	int m_screenWidth;
	int m_screenHeight;
	glm::vec3 m_position;
	glm::vec3 m_rotation;

	glm::mat4 m_projectionMatrix;
	glm::mat4 m_viewMatrix;
};

#endif