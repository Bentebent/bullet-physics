#ifndef SOURCE_GRAPHICS_MESH_MESH_HPP
#define SOURCE_GRAPHICS_MESH_MESH_HPP

#include <GLFW/glew.h>

struct StaticMesh
{
	GLuint vao;
	GLuint ibo;
	unsigned int indexCount;
};

#endif