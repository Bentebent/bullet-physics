#ifndef SOURCE_GRAPHICS_MESH_VERTEX_HPP
#define SOURCE_GRAPHICS_MESH_VERTEX_HPP

#include <glm/glm.hpp>

struct StaticVertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 uv;
};

#endif