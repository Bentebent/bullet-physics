#include "MeshLoader.hpp"
#include "Vertex.hpp"

#include <iostream>
#include <fstream>
#include <vector>
#include <GLFW/glew.h>

MeshLoader::MeshLoader()
{
	m_scene = nullptr;
}

MeshLoader::~MeshLoader()
{

}

StaticMesh MeshLoader::LoadStaticMesh(const char* filepath)
{

	std::ifstream file(filepath, std::ios::in);

	if (!file.fail())
	{
		file.close();
	}
	else
	{
		std::cout << "Couldn't open file: " << filepath << std::endl;
	}

	m_scene = m_importer.ReadFile(filepath, aiProcessPreset_TargetRealtime_Quality);

	if (!m_scene)
	{
		std::cout << m_importer.GetErrorString() << std::endl;
	}

	std::cout << "Scene imported successfully: " << filepath << std::endl;

	return GenerateBuffers();
}

StaticMesh MeshLoader::GenerateBuffers()
{
	StaticMesh sm;

	for (int i = 0; i < m_scene->mNumMeshes; i++)
	{
		const aiMesh* mesh = m_scene->mMeshes[i];

		//Load index data
		int numFaces = mesh->mNumFaces;
		
		unsigned int faceIndex = 0;
		std::vector<unsigned int> faces;
		for (int t = 0; t <numFaces; t++)
		{
			const aiFace* face = &mesh->mFaces[t];

			for (int u = 0; u < 3; u++)
				faces.push_back(face->mIndices[u]);
		}

		std::vector<StaticVertex> vertices;

		int vertexCount = mesh->mNumVertices;

		for (int i = 0; i < vertexCount; i++)
		{
			StaticVertex v;
			v.position.x = mesh->mVertices[i].x;
			v.position.y = mesh->mVertices[i].y;
			v.position.z = mesh->mVertices[i].z;

			v.normal.x = mesh->mNormals[i].x;
			v.normal.y = mesh->mNormals[i].y;
			v.normal.z = mesh->mNormals[i].z;

			v.uv.x = mesh->mTextureCoords[0][i].x;
			v.uv.y = mesh->mTextureCoords[0][i].y;
			vertices.push_back(v);
		}

		//Generate IBO
		GLuint ibo;
		glGenBuffers(1, &ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * faces.size(), faces.data(), GL_STATIC_DRAW);

		//Generate VBO
		GLuint vbo;
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertexCount * sizeof(StaticVertex), vertices.data(), GL_STATIC_DRAW);

		//Generate VAO
		GLuint vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);

		// position data
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)0);

		// normal data
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(3 * sizeof(float)));

		// uv data
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(StaticVertex), (void*)(6 * sizeof(float)));

		glBindVertexArray(0);

		sm.ibo = ibo;
		sm.vao = vao;
		sm.indexCount = faces.size();
	}

	return sm;
}