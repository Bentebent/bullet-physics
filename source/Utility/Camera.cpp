#include "Camera.hpp"
#include <glm/ext.hpp>
Camera::Camera()
{
	m_viewMatrix = glm::mat4(1.0f);
	m_projectionMatrix = glm::mat4(1.0f);
}

Camera::~Camera()
{

}

void Camera::SetProjectionMatrix(float fov, float nearClip, float farClip, int screenWidth, int screenHeight)
{
	m_screenHeight = screenHeight;
	m_screenWidth = screenWidth;
	m_fieldOfView = fov;
	m_nearClip = nearClip;
	m_farClip = farClip;

	m_projectionMatrix = glm::perspective<float>(fov, screenWidth / screenHeight, nearClip, farClip);
}

glm::mat4 Camera::GetProjectionMatrix()
{
	return m_projectionMatrix;
}

glm::mat4 Camera::GetViewMatrix()
{
	return m_viewMatrix;
}