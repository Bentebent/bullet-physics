#ifndef SOURCE_GRAPHICS_SHADER_SHADERBASEMODEL_HPP
#define SOURCE_GRAPHICS_SHADER_SHADERBASEMODEL_HPP

#include <GLFW/glew.h>
#include <GLFW/glfw3.h>

#include <map>
#include <string>

class ShaderBaseModel
{
	friend class ShaderManager;

private:
	std::map<std::string, GLuint>* m_shader;
	std::map<std::string, GLuint>* m_shaderProgram;

	ShaderBaseModel();
	~ShaderBaseModel();

public:

	GLuint GetShaderID(std::string shaderKey);
	GLuint GetShaderProgram(std::string shaderProgramKey);
	bool SaveShader(std::string shaderKey, GLuint shaderID);
	bool SaveShaderProgram(std::string shaderProgramKey, GLuint shaderProgramID);

	void DeleteShader(std::string shaderKey);
	void DeleteShaderProgram(std::string shaderProgramKey);
};

#endif