#include <iostream>

#include "InitializeGLFW.hpp"

#include "Utility/GLFWInput.hpp"
#include "Utility/Camera.hpp"

#include "Graphics/Shader/ShaderManager.hpp"
#include "Graphics/Mesh/MeshLoader.hpp"
#include "Graphics/Mesh/Mesh.hpp"

int main(int argc, char** argv)
{
	GLFWwindow* window;
	InitializeGLFW(&window, 1280, 720);
	
	GLFWInput* input = new GLFWInput(window);

	Camera camera;
	camera.SetProjectionMatrix(45.0f, 1.0f, 1000.0f, 1280, 720);

	MeshLoader ml;
	StaticMesh test;
	test = ml.LoadStaticMesh("../assets/models/cube.obj");

	GLint proj;
	GLint view;

	ShaderManager sm;
	sm.CreateProgram("SimpleGeometry");
	sm.LoadShader("../assets/shaders/SimpleGeometryVS.glsl", "VS", GL_VERTEX_SHADER);
	sm.LoadShader("../assets/shaders/SimpleGeometryFS.glsl", "FS", GL_FRAGMENT_SHADER);
	sm.AttachShader("VS", "SimpleGeometry");
	sm.AttachShader("FS", "SimpleGeometry");
	sm.LinkProgram("SimpleGeometry");
	
	proj = glGetUniformLocation(sm.GetShaderProgramID("SimpleGeometry"), "projection");
	view = glGetUniformLocation(sm.GetShaderProgramID("SimpleGeometry"), "view");

	while (!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		if (input->IsKeyPressedOnce(GLFW_KEY_ESCAPE))
			break;

		sm.UseProgram("SimpleGeometry");
		
		glm::mat4 projMatrix = camera.GetProjectionMatrix();
		glm::mat4 viewMatrix = camera.GetViewMatrix();
		
		glUniformMatrix4fv(proj, 1, false, &projMatrix[0][0]);
		glUniformMatrix4fv(view, 1, false, &viewMatrix[0][0]);
		
		glBindVertexArray(test.vao);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, test.ibo);
		glDrawElements(GL_TRIANGLES, test.indexCount, GL_UNSIGNED_INT, NULL);
	
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	
	glfwTerminate();
	return 0;
}