------------------------------------------------------------------
-- premake 4 test solution
------------------------------------------------------------------
solution "BulletPhysics"
 
    ------------------------------------------------------------------
    -- setup common settings
    ------------------------------------------------------------------
    configurations { "Debug", "Release" }
    flags { "Unicode", "NoPCH",  "FloatFast" }
	libdirs{ "lib" }
    includedirs { "source" }
    location "build"
 
    ------------------------------------------------------------------
    -- setup the build configs
    ------------------------------------------------------------------
    configuration "Debug"
        defines { "DEBUG" }
        flags { "Symbols" }
        targetsuffix "_d"
		targetdir ( "bin/debug" )

    configuration "Release"
        defines { "NDEBUG" }
        flags { "Optimize" }
		targetdir ( "bin/release" ) 
 
    ------------------------------------------------------------------
    -- Executable project
    ------------------------------------------------------------------
    project "BulletPhysics"
        kind "ConsoleApp"
        language "C++"
        targetdir "bin"
        files { "source/**.h", "source/**.hpp", "source/**.cpp", "assets/shaders/**.glsl" }
		includedirs{ "include" }

		configuration {"debug"}
			links{"glfw3", "glew32", "glfw3dll", "opengl32", "BulletCollision_debug", "BulletDynamics_debug", "BulletSoftBody_debug", "ConvexDecomposition_debug", "HACD_debug", "LinearMath_debug", "OpenGLSupport_debug", "assimp"}
			
		configuration {"Release"}
			links{"glfw3", "glew32", "glfw3dll", "opengl32", "BulletCollision", "BulletDynamics", "BulletSoftBody", "ConvexDecomposition", "HACD", "LinearMath", "OpenGLSupport", "assimp"}